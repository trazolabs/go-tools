package rut

import (
	"strconv"
	"strings"
)

func ValidateRut(rut string) bool {
	rut = strings.ToLower(rut)

	if !strings.Contains(rut, "-") {
		return false
	}

	// remove punctuation.
	rut = strings.Replace(rut, ".", "", -1)

	// separate rut.
	parts := strings.Split(rut, "-")
	digitsString := parts[0]
	validator := parts[1]

	if len(validator) != 1 {
		return false
	}

	digits, err := stringToIntArray(digitsString)
	if err != nil {
		return false
	}

	// multiply values.
	factor := 2
	sum := 0
	for i := len(digits) - 1; i >= 0; i-- {
		sum += digits[i] * factor

		factor += 1
		if factor > 7 {
			factor = 2
		}
	}

	mod := sum % 11
	rest := 11 - mod

	result := strconv.Itoa(rest)
	if rest == 11 {
		result = "0"
	}
	if rest == 10 {
		result = "k"
	}

	return validator == result
}

func stringToIntArray(v string) ([]int, error) {
	var digits []int

	for _, char := range v {
		digit, err := strconv.Atoi(string(char))
		if err != nil {
			return nil, err
		}

		digits = append(digits, digit)
	}

	return digits, nil
}
