package notification

import (
	"bytes"
	"encoding/json"
	"errors"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httputil"
	"os"
)

type device struct {
	UserIDs []uint `json:"userIds"`
	Title   string `json:"title"`
	Body    string `json:"body"`
}

type email struct {
	To         []string          `json:"to"`
	Subject    string            `json:"subject"`
	Data       map[string]string `json:"data"`
	TemplateID string            `json:"templateId"`
}

type notification struct {
	Email  *email  `json:"email"`
	Device *device `json:"device"`
}

type Builder struct {
	hasEmail bool
	email    *email

	hasDevice bool
	device    *device
}

func New() *Builder {
	return &Builder{
		email:  &email{},
		device: &device{},
	}
}

func (builder *Builder) SetupEmail(templateID, subject string, data map[string]string) {
	builder.hasEmail = true
	builder.email.Subject = subject
	builder.email.TemplateID = templateID
	builder.email.Data = data
}

func (builder *Builder) AddEmail(email string) {
	builder.hasEmail = true
	builder.email.To = append(builder.email.To, email)
}

func (builder *Builder) SetupDevice(title, body string) {
	builder.hasDevice = true
	builder.device.Title = title
	builder.device.Body = body
}

func (builder *Builder) AddUser(userID uint) {
	builder.hasDevice = true
	builder.device.UserIDs = append(builder.device.UserIDs, userID)
}

func (builder *Builder) Send() error {
	notification := builder.build()

	dataBytes, err := json.Marshal(notification)
	if err != nil {
		return err
	}

	response, err := http.Post(getWMWServer(), "application/json", bytes.NewBuffer(dataBytes))
	if err != nil {
		return err
	}

	if os.Getenv("DEBUG") == "true" {
		raw, _ := httputil.DumpResponse(response, true)
		log.Println(string(raw))
	}

	if response.StatusCode < 200 || response.StatusCode >= 400 {
		data, err := ioutil.ReadAll(response.Body)
		if err != nil {
			return err
		}
		return errors.New("error while sending notification: " + string(data))
	}

	return nil
}

func getWMWServer() string {
	url, ok := os.LookupEnv("WMW_API")
	if !ok {
		return "http://wmw:5000/notify"
	}
	return url
}

func (builder *Builder) build() *notification {
	notification := &notification{}

	if builder.hasEmail {
		notification.Email = builder.email
	}

	if builder.hasDevice {
		notification.Device = builder.device
	}

	return notification
}
