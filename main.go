package main

import (
	"gitlab.com/trazolabs/go-tools/events"
	"gitlab.com/trazolabs/go-tools/notification"
	"log"
	"time"
)

/**
 For some reason I cannot test with "go test", so this is a workaround...
*/

type User struct {
	Username string
	Rut      string
}

func TestPushEvent() {
	user := &User{
		Username: "testing",
		Rut:      "rut",
	}

	events.PushEvent(nil, "USERS", "create", user, "created a new user %s", "testing")
	time.Sleep(5 * time.Second)
}

func TestNotification() {
	builder := notification.New()
	builder.SetupEmail("d-fa98ad7099a347fca1aebd588f413ff5", "Hola, ñañ", map[string]string{
		"treasureLocation": "the cabin",
	})
	builder.AddEmail("hernan@trazolabs.com")

	builder.SetupDevice("Hola, ñañ", "Revisa tu correo...")
	builder.AddUser(2)

	if err := builder.Send(); err != nil {
		log.Fatal(err)
	}
}

func main() {
	TestNotification()
}
