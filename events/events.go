package events

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httputil"
	"os"
)

type event struct {
	Tag       string      `json:"tag"`
	Action    string      `json:"action"`
	Humanized string      `json:"humanized"`
	Payload   interface{} `json:"payload"`
	UserToken string      `json:"userToken"`
}

func PushEvent(r *http.Request, tag, action string, metadata interface{}, humanized string, a ...interface{}) {
	userToken := ""
	if r != nil {
		userToken = r.Header.Get("Authorization")
	}

	go pushEvent(event{
		Tag:       tag,
		Action:    action,
		UserToken: userToken,
		Humanized: fmt.Sprintf(humanized, a...),
		Payload:   metadata,
	})
}

func pushEvent(event event) {
	eventsUrl := getEventsUrl()

	data, err := json.Marshal(event)
	if err != nil {
		fmt.Println("error marshaling event:", err, fmt.Sprintf("(event is: %s)", event.Humanized))
		return
	}

	res, err := http.Post(eventsUrl, "application/json", bytes.NewBuffer(data))
	if err != nil {
		fmt.Println("error while pushing event:", err, fmt.Sprintf("(event is: %s)", event.Humanized))
		fmt.Println("EVENTS_API is", eventsUrl)
		return
	}

	if res.StatusCode != 200 {
		fmt.Println("error while pushing event:", event.Humanized)
		response, err := httputil.DumpResponse(res, true)
		if err != nil {
			fmt.Println("error dumping response:", err.Error())
		}
		fmt.Println(string(response))
		return
	}
}

func getEventsUrl() string {
	value, ok := os.LookupEnv("EVENTS_API")
	if !ok {
		return "http://events:5000/events"
	}
	return value
}
